var mongoose = require('mongoose'),
    databaseURL = 'mongodb://dbuser:dbpassword@ds119078.mlab.com:19078/polling-app';

mongoose.Promise = global.Promise;

mongoose.connect(databaseURL)
    .then(() => {
        console.log('Database Connected');
    })
    .catch((err) => {
        console.log('Error in database connection: ' + err);
    });