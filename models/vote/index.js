var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var voteSchema = new Schema({
    os: {
        type: String,
        required: true
    },
    points: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('Vote', voteSchema);