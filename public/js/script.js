var voteForm = document.getElementById('vote-form');
var audio = document.getElementById("myaudio");
audio.volume = 0.03;

voteForm.addEventListener('submit', (e) => {
    
    var choice = document.querySelector('input[name=os]:checked').value;
    console.log('Choice: ' + choice);
    var data = {
        os: choice
    }

    fetch('/data', {
        method: 'post',
        body: JSON.stringify(data),
        headers: new Headers({
           'Content-Type': 'application/json' 
        })
    })
        .then(res => res.json())
        .then(data => console.log(data))
        .then(err => console.log(err));

    e.preventDefault();
});

fetch('/data')
    .then(res => res.json())
    .then(data => {
        var votes = data.votes;
        var totalVotes = votes.length;
        const voteCounts = votes.reduce((acc, vote) => ((acc[vote.os] = (acc[vote.os] || 0) + parseInt(vote.points)), acc), {});
        
        let dataPoints = [
            { label: 'Windows', y: voteCounts.Windows },
            { label: 'MacOS', y: voteCounts.MacOS },
            { label: 'Linux', y: voteCounts.Linux },
            { label: 'Other', y: voteCounts.Other }
        ];
        
        var chartContainer = document.querySelector('#chartContainer');
        
        if (chartContainer) {
            var chart = new CanvasJS.Chart('chartContainer', {
                animationEnabled: true,
                theme: 'theme1',
                title: {
                    text: 'Poll Result'
                },
                data: [
                    {
                        type: 'column',
                        dataPoints: dataPoints
                    }
                ]
            });
            chart.render();
        
            Pusher.logToConsole = true;
        
            var pusher = new Pusher('8cf4b7f5edbdb3ec6c0c', {
              cluster: 'ap2',
              encrypted: true
            });
        
            var channel = pusher.subscribe('os-poll');
            channel.bind('os-vote', function(data) {
                console.log('Data');
                console.log(data);
        
              dataPoints = dataPoints.map((x) => {
                console.log('X.label: ');
                console.log(x.label);
                console.log('Data.os: ');
                console.log(data.os);
                if(x.label == data.os) {
                    x.y += data.points;
                    return x;
                } else {
                    return x;
                }
              });
              chart.render();
            });
        }
    });