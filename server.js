var express = require('express'),
    path = require('path'),
    bodyParser = require('body-parser'),
    cors = require('cors'),
    app = express(),
    expressHandlebars = require('express-handlebars'),
    session = require('express-session'),
    passport = require('passport'),
    port = process.env.PORT || 3000;

require('./config/database');

var pollRoute = require('./routes/poll');
var authRoute = require('./routes/auth');
var dataRoute = require('./routes/data');

require('./config/passport');

app.engine('.hbs', expressHandlebars({
    defaultLayout: 'layout',
    extname: '.hbs'
}));
app.set('view engine', '.hbs');

app.use(session({
    secret: 'eminem', // session secret
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

app.use(function (req, res, next) {
    res.locals.login = req.isAuthenticated();
    res.locals.user = req.user;
    console.log('Locals');
    console.log(req.user);
    res.locals.id = req.user;
    res.locals.session = req.session;
    next();
});

app.use('/', pollRoute);
// app.use('/join', authRoute);
app.use('/data', dataRoute);

app.set('port', port);

app.listen(port, () => {
    console.log('App is running on port: ' + port);
});

function isLoggedIn(req, res, next) {
    if(req.isAuthenticated()) {
        console.log('Server')
        console.log(req.user);
        return next();
    }
    else res.redirect('/join');
}