var express = require('express'),
    router = express.Router(),
    passport = require('passport');

router.get('/', (req, res, next) => {
    res.render('auth', {});
});

router.get('/logout', isLoggedIn, function (req, res, next) {
    req.session.isUser = false;
    req.logout();
    res.redirect('/join');
});

router.get('/auth/facebook', passport.authenticate('facebook'));
router.get('/login/facebook/return', passport.authenticate('facebook',
    { failureRedirect: '/join' }),
    function(req, res) {
        // Successful authentication, redirect home.
        req.session.userId = req.user._id;
        req.session.username = req.user.name;
        res.redirect('/poll');
});

router.get('/auth/google', passport.authenticate('google', { scope: ['profile'] }));
router.get('/login/google/return', passport.authenticate('google',
    { failureRedirect: '/join' }),
    function(req, res) {
        // Successful authentication, redirect home.
        req.session.userId = req.user._id;
        req.session.username = req.user.name;
        res.redirect('/poll');
});

module.exports = router;

function isLoggedIn(req, res, next) {
    if(req.isAuthenticated()) {
        return next();
    }
    else res.redirect('/join');
}