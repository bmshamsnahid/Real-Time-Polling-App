var express = require('express'),
    router = express.Router(),
    Pusher = require('pusher'),
    mongoose = require('mongoose'),
    Vote = require('../../models/vote');

var pusher = new Pusher({
  appId: '465728',
  key: '8cf4b7f5edbdb3ec6c0c',
  secret: 'acc3d67333e44f5cd4a9',
  cluster: 'ap2',
  encrypted: true
});

router.get('/', (req, res, next) => {
    Vote.find((err, votes) => {
        if (err) throw err;
        else {
            res.json({
                success: true,
                votes: votes
            });
        }
    });
});

router.post('/', (req, res, next) => {
    
    console.log('User Info');
    console.log(req.session.userId);

    var newVote = new Vote({
        'os': req.body.os,
        'points': 1
    });

    newVote.save((err, vote) => {
        if (err) throw err;
        else {
            pusher.trigger('os-poll', 'os-vote', {
                "points": parseInt(vote.points),
                'os' : vote.os
            });
            return res.json({
                success: true,
                message: 'Thank you for voting'
            });
        }
    });
});

module.exports = router;

function isLoggedIn(req, res, next) {
    if(req.isAuthenticated()) {
        console.log('In router');
        console.log(req.user);
        return next();
    }
    else res.redirect('/join');
}