var express = require('express'),
    router = express.Router(),
    Pusher = require('pusher'),
    mongoose = require('mongoose'),
    Vote = require('../../models/vote');

router.get('/', (req, res, next) => {
    Vote.find((err, votes) => {
        if (err) throw err;
        else {
            res.render('index', {});
        }
    });
});

module.exports = router;